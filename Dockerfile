FROM alpine

ENV SYNCTHING_VERSION=0.14.38 \
    STNOUPGRADE=true

ADD start.sh /start.sh

RUN cd /tmp &&\
    apk -U add openssl gnupg && \
    echo "Getting GPG keys for Syncthing" && \
    gpg-agent --daemon && \
    gpg --quiet --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 49F5AEC0BCE524C7 D26E6ED000654A3E && \
    echo "Getting Syncthing and its signature" && \
    wget -q https://github.com/syncthing/syncthing/releases/download/v${SYNCTHING_VERSION}/sha1sum.txt.asc && \
    wget -q https://github.com/syncthing/syncthing/releases/download/v${SYNCTHING_VERSION}/syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz && \
    echo "Checking Syncthing sha1 sum signature" && \
    gpg --quiet --verify sha1sum.txt.asc && \
    echo "Checking Syncthing sha1 checksum" && \
    grep syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz sha1sum.txt.asc | sha1sum -c - && \
    echo "Installing syncthing" && \
    tar -xzf syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz syncthing-linux-amd64-v${SYNCTHING_VERSION}/syncthing && \
    mv syncthing-linux-amd64-v${SYNCTHING_VERSION}/syncthing /bin/ && \
    echo "Cleaning up" && \
    rm -rf /tmp/* && \
    rm -rf /root/* && \
    apk del gnupg openssl && \
    rm -rf /var/lib/apt/lists/* && \
    chmod +x /start.sh && mkdir -p /srv/config /srv/data

EXPOSE 8384 22000 21027/udp

ENTRYPOINT ["/start.sh"]
