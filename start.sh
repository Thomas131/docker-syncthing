#!/bin/sh
if [[ ! -f /srv/config/config.xml ]]; then
    echo "Config is not found, generating"
    /bin/syncthing -generate="/srv/config"
    sed -e "s/id=\"default\" path=\"\/root\/Sync\"/id=\"default\" path=\"\/srv\/data\/default\"/" -i /srv/config/config.xml
    sed -e "s/<address>127.0.0.1:8384/<address>0.0.0.0:8384/" -i /srv/config/config.xml
fi

exec /bin/syncthing -home "/srv/config" -no-browser $@
